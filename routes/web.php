<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/stock/list/{request_type}','HomeController@getStockList');
Route::get('/stock/myStock','HomeController@getMyStocks');
Route::get('/stock/graphData/{com_id}','HomeController@getCompanyStockHistory');
Route::get('/stock/stockDetails/{com_id}','HomeController@getLiveStockDetails');

Route::post('/stock/purchase','HomeController@postPurchaseStock');
