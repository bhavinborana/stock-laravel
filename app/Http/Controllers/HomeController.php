<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Company;
use App\Stock;
use App\Transaction;
use App\User;

use DataTables;
use DB;
use Auth;

class HomeController extends Controller
{
    const TRANSACTION_TYPE_BUY = 1;
    const TRANSACTION_TYPE_SELL = 2;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * To get stock data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStockList(){
        $results =  Company::getCompanies();
        
        return DataTables::of($results)
                ->addColumn('price', function($results) {
                    if(!isset($results->stock) || empty($results->stock->toArray()))
                    {
                        return "0";
                    }
                    else{
                        return $results->stock[0]->price;
                    }
                })
                ->addColumn('growth', function($results) {
                    if(!isset($results->stock) || empty($results->stock->toArray()))
                    {
                        return "0";
                    }
                    else{
                        $earnings = $results->stock[0]->price - $results->stock[1]->price;
                        $percentGain = ($earnings*100)/$results->stock[1]->price; 
                        return round($percentGain,1)."%";
                    }
                })
                ->addColumn('action', 'actions')
                ->make(TRUE); 
    }

    /**
     * To get my stock data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyStocks(){
        $results =  Company::getUserStock();
        
        return DataTables::of($results)
                ->addColumn('transaction_type',function($results){
                    if($results->transaction_type == self::TRANSACTION_TYPE_BUY){
                        return "Buy";
                    }
                    else{
                        return "Sell";
                    }
                })
                ->addColumn('action', 'my_actions')
                ->make(TRUE); 
    }

    /**
     * To get my stock history for graph.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCompanyStockHistory($id){
        $results = Stock::where('company_id',$id)->select('price')->selectRaw('DATE(timestamp) as dateStock')->groupBy(DB::raw('DATE(timestamp)'))->get();
        if(isset($results) && !empty($results)){
            echo json_encode($results);
        }
        else{
            echo 0; 
        }
        exit;
    }

    /**
     * To get my stock details.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLiveStockDetails($id){
        $results = Stock::where('company_id',$id)->select('price','name','stocks.company_id')->join('companies','companies.id','=','stocks.company_id')->orderBy('timestamp','desc')->first();
        $userBalance = Auth::user()->total_balance;
        if(isset($results) && !empty($results)){
            $results->balance = $userBalance;
            echo json_encode($results);
        }
        else{
            echo 0; 
        }
        exit;
    }

    /**
     * To purchase stock.
     *
     * @return \Illuminate\Http\Response
     */
    public function postPurchaseStock(Request $request){
        $validation = Validator::make($request->all(), [
            'companyId' => 'required|integer',
            'totalUnit' => 'required',
            'transactionType' => 'required|integer|min:1|max:2'
        ]);

        if($validation->errors()->all()){
            $errorMsg = "";
            foreach ($validation->errors()->all() as $key => $value) {
                $errorMsg .= $value;
            }

            $response = ['success' => 0, 'message' => $errorMsg];
            echo json_encode($response);
            exit;
        }
        else{
            $userBalance = Transaction::whereDate('transactions.created_at',date('Y-m-d'))
                                            ->whereUserId(Auth::user()->id)
                                            ->selectRaw('count(transactions.id) as totalTransactions')
                                            ->selectRaw('GROUP_CONCAT(transactions.company_id) as compStock')
                                            ->groupBy('user_id')
                                            ->first();

            if($request->get('transactionType') == self::TRANSACTION_TYPE_SELL){
                $totalBought = 0;
                $totalSold = 0;
                    
                $checkShareBalance = Transaction::whereUserId(Auth::user()->id)
                                            ->select('transaction_type')
                                            ->selectRaw('sum(transactions.total_share) as totalShare')
                                            ->whereCompanyId($request->get('companyId'))
                                            ->groupBy('transaction_type')
                                            ->get();

                if(isset($checkShareBalance) && !empty($checkShareBalance)){
                    foreach ($checkShareBalance as $key => $value) {
                        if($value->transaction_type == self::TRANSACTION_TYPE_SELL){
                            $totalSold = $value->totalShare;
                        }
                        else{
                            $totalBought = $value->totalShare;
                        }
                    }
                }
                $balance = $totalBought - $totalSold;
            }

            $userModel = User::whereId(Auth::user()->id)->select('total_balance')->first();

            $stockServerPrice = Stock::where('company_id',$request->get('companyId'))->select('price')->orderBy('timestamp','desc')->first();
           
            if(isset($stockServerPrice->price)){
                
                if(is_object($userBalance) && $request->get('transactionType') == self::TRANSACTION_TYPE_BUY && $userModel->total_balance < ($request->get('totalUnit')*$stockServerPrice->price)){
                    $response = ['success' => 0, 'message' => 'User does not have sufficient balance'];
                    echo json_encode($response);
                    exit;
                }
                elseif($request->get('transactionType') == self::TRANSACTION_TYPE_SELL && $balance <  $request->get('totalUnit')){
                    $response = ['success' => 0, 'message' => 'User does not have sufficient stock balance to sold'];
                    echo json_encode($response);
                    exit;
                }
                elseif(is_object($userBalance) && $userBalance->totalTransactions >= 3){
                    $response = ['success' => 0, 'message' => 'You have already made maximum transactions.' ];
                    echo json_encode($response);
                    exit;
                }
                elseif(is_object($userBalance) && ($userBalance->totalTransactions >= 3 || in_array($request->get('companyId'), explode(',', $userBalance->compStock)))){
                    $response = ['success' => 0, 'message' => 'You have already made maximum transactions.' ];
                    echo json_encode($response);
                    exit;
                }
                else{
                    $insert = Transaction::insert([ 'user_id' => Auth::user()->id,
                                                    'unit_price' => $stockServerPrice->price, 
                                                    'company_id' => $request->get('companyId'), 
                                                    'total_share' => $request->get('totalUnit'), 
                                                    'transaction_type' => $request->get('transactionType'),
                                                    'total_price' => $request->get('totalUnit')*$stockServerPrice->price
                                                ]);
                    if($request->get('transactionType') == self::TRANSACTION_TYPE_BUY){
                        $availableBal = $userModel->total_balance-($request->get('totalUnit')*$stockServerPrice->price);
                    }
                    else{
                        $availableBal = $userModel->total_balance+($request->get('totalUnit')*$stockServerPrice->price);
                    }
                    User::whereId(Auth::user()->id)->update(['total_balance' => $availableBal]);
                    $response = ['success' => 1, 'message' => 'Transaction successfull' ];
                    echo json_encode($response);
                    exit;
                }
            }
            else{
                $response = ['success' => 0, 'message' => 'Stock price does not found.' ];
                echo json_encode($response);
                exit;
            }
        } 
        exit;
    }
}
