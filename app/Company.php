<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code'
    ];

    public function stock(){
    	return $this->hasMany('App\Stock', 'company_id', 'id');
    }

    public static function getCompanies(){
    	return  Company::select('companies.id','companies.name')
                        ->with(['stock' => function($query){
                            $query->orderBy('timestamp','desc');
                        }]);
    }

    public static function getUserStock(){
    	return  Company::select('companies.id','companies.name','transactions.unit_price','transactions.total_share','transaction_type')
                        ->join('transactions','transactions.company_id','=','companies.id')
                        ->join('users','transactions.user_id','=','users.id')
                        ->where('transactions.user_id','=',Auth::user()->id)
                        ->with(['stock' => function($query){
                            $query->orderBy('timestamp','desc');
                        }]);
	}
}
