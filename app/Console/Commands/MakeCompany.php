<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DOMDocument;
use App\Company;
use Log;
class MakeCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:company';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to add or update company name with code.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stockLink       = 'http://borsadefteri.blogspot.com/p/hisse-senedi-kodlar.html';
        $htmlData   = file_get_contents($stockLink);
        libxml_use_internal_errors(true);

        $domObj = new DOMDocument;
        $domObj->loadHTML($htmlData);
       
        $outer = $domObj->getElementById('post-body-6227156483018901707');
        $tables = $domObj->getElementsByTagName('table');
        $tr     = $domObj->getElementsByTagName('tr');
        foreach ($tr as $i => $companyRecord) { 
            if($companyRecord->getElementsByTagName('td')->length){
                $companyCode = @$companyRecord->getElementsByTagName('td')->item(0)->firstChild->data;
                $companyName = @$companyRecord->getElementsByTagName('td')->item(1)->firstChild->data;

                if(!empty(trim($companyName)) && !empty(trim($companyCode))){
                    $companyCheck = Company::firstOrCreate(['code'=>trim($companyCode),'name' => trim($companyName)]);
                }
            }  
        }

        exit;
    }
}
