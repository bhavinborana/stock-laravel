<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Company;
use App\Stock;
use Log;

class UpdateStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to updated the stock prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new \GuzzleHttp\Client(['base_uri' => 'https://portal-widgets-v3.foreks.com']);

            $companies = Company::all();

            if(isset($companies) && !empty($companies)){
                foreach ($companies as $key => $value) {
                    $res = $client->post("/periodic-tabs/data",[
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            "Date" => date('Y-m-d H:i:s')
                        ],
                        'form_params' => ["code" => $value->code.".E.BIST","id" => "graph1","period" => "1Y","intraday" => 1440, "delay" => 15, "linecolor" => "#ffd355"]
                    ]);

                    $json = $res->getBody()->getContents();

                    $array = json_decode($json);

                    if(isset($array->data) && !empty($array->data)){
                        foreach ($array->data as $key => $stockValue) {
                            $companyCheck = Stock::firstOrCreate(['timestamp'=>trim(date('Y-m-d H:i:s',$stockValue[0]/1000)),'price' => trim($stockValue[1]),'company_id' => $value->id]);
                        }
                    }
                }
            }
            
        } catch (Exception $e) {
            Log::error($e->getMessage()." on line number ".$e->getLine());
        }

        exit;

    }
}
