 <div class='btn-group'>
    <a href="javascript::void(0)" class='btn btn-default btn-s zoom' onclick="loadGraph({{$id}})" title="View Graph">
        Graph
    </a>
    @if($transaction_type == 1)
    <a href="javascript::void(0)" class='btn btn-danger btn-s zoom' onclick="sellMe({{$id}})" title="Sell Stock">
        Sell
    </a>
    @else
    <a href="javascript::void(0)" class='btn btn-success btn-s zoom' onclick="purchaseMe({{$id}})" title="Purchase Stock">
        Buy
    </a>
    @endif
</div>
