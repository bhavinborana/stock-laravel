@extends('layouts.app')
<style type="text/css">
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <section class="col-lg-12 connectedSortable">
                        <div class="col-lg-6">
                          <!-- Custom tabs (Charts with tabs)-->
                          <div class="nav-tabs-custom">
                            <!-- Tabs within a box -->
                            <ul class="nav nav-tabs pull-right">
                              <li class="active"><a href="#all-stock" data-toggle="tab">All</a></li>
                              <li><a href="#owned-stock" data-toggle="tab" onclick="reloadTransaction()">My Transactions</a></li>
                              <li class="pull-left header"><i class="fa fa-inbox"></i> Stock</li>
                            </ul>
                            <div class="tab-content no-padding">
                                <div class="chart tab-pane active" id="all-stock" style="position: relative;">
                                    <table id="allStockTbl" class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Performance</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="chart tab-pane" id="owned-stock" style="position: relative;">
                                    <table id="ownStockTbl" class="table table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Unit Price</th>
                                                <th>Total Unit</th>
                                                <th>Purchase Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                          </div>
                          <!-- /.nav-tabs-custom -->
                        </div>
                        <div class="col-lg-6">
                          <div class="box box-solid bg-teal-gradient">
                            <div class="box-header">
                              <i class="fa fa-th"></i>
                              <h3 class="box-title">Performance Graph</h3>
                            </div>
                            <div class="box-body border-radius-none">
                              <div class="chart" id="line-chart" style="height: 250px;"></div>
                            </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
        <form action="{{ url('stock/purchase') }}" type="post" id="buyForm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="actionType">Buy</span> Stock</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-success alert-dismissible" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    <p class="customSuccess"></p>
                </div>
                <div class="alert alert-danger alert-dismissible" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Error!</h4>
                    <p class="customError"></p>
                </div>
                <p>Are you sure you want to <span class="actionType">buy</span> <span class="company-span"></span>'s stock with price of <span class="price-span"></span> per unit?</p>
                <br>
                <p>Available Balance: <span class="available-balace"></span></p>
                <br>
                Unit Price
                <input type="text" readonly="true" name="unitPrice" id="unitPrice" value="">
                X
                <input type="number" name="totalUnit" id="totalUnit" required="true" value="">
                <input type="hidden" name="companyId" id="companyId" value="">
                <input type="hidden" name="transactionType" id="transactionType" value="1">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary actionType">Buy</button>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection
<!-- jQuery 3 -->
@section('scripts')
<script>
    $(function () {
            $('#allStockTbl').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                 paging: true,
                "autoWidth": false,
                "pageLength": 10,
                "orderable": false, 
                ajax: { 
                    "url": "stock/list/1"
                },
                "aoColumns": [
                    {"data":"name","name":"name","orderable": false},
                    {"data":"price","name":"price","orderable": false},
                    {"data":"growth","name":"growth","orderable": false},
                    {"data":"action","name":"action","orderable": false}
                ]
            });

            $('#ownStockTbl').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                 paging: true,
                "autoWidth": false,
                "pageLength": 10,
                "orderable": false, 
                ajax: { 
                    "url": "stock/myStock"
                },
                "aoColumns": [
                    {"data":"name","name":"name","orderable": false},
                    {"data":"unit_price","name":"unit_price","orderable": false},
                    {"data":"total_share","name":"total_share","orderable": false},
                    {"data":"transaction_type","name":"transaction_type","orderable": false},
                    {"data":"action","name":"action","orderable": false}
                ]
            });
        });

    function loadGraph(id){
        jQuery.ready();
        $.ajax({
            url: "stock/graphData/"+id,
            dataType: "json",
            success: function(data){
                $('#line-chart').html("");

                var line = new Morris.Line({
                    element: 'line-chart',
                    resize: true,
                    data: data,
                    xkey: 'dateStock',
                    ykeys: ['price'],
                    labels: ['Stock Price'],
                    lineColors: ['black'],
                    lineWidth: 2,
                    hideHover: 'auto',
                    gridTextColor: "#fff",
                    gridStrokeWidth: 0.4,
                    pointSize: 4,
                    pointStrokeColors: ["#efefef"],
                    gridLineColor: "#efefef",
                    gridTextFamily: "Open Sans",
                    gridTextSize: 10,
                    gridIntegers: true,
                  });
            },
            error: function(){
                $('#line-chart').html("No data available");
            }
        });
    }

    function purchaseMe(id){
        $('#totalUnit').val("");
        $('.alert-success').hide();
        $('.alert-danger').hide();
        $('.actionType').html('Buy');
        $('#transactionType').val(1);
        $.ajax({
            url: "stock/stockDetails/"+id,
            dataType: "json",
            success: function(data){
                $('.company-span').html(data.name);
                $('.price-span').html(data.price);
                $('#unitPrice').val(data.price);
                $('#companyId').val(data.company_id);
                $('.available-balace').html(data.balance);
                $('#modal-default').modal('show');
            },
            error: function(){
                $('#line-chart').html("No data available");
            }
        });
    }

    function sellMe(id){
        $('#totalUnit').val("");
        $('.alert-success').hide();
        $('.alert-danger').hide();
        $('.actionType').html('Sell');
        $('#transactionType').val(2);
        $.ajax({
            url: "stock/stockDetails/"+id,
            dataType: "json",
            success: function(data){
                $('.company-span').html(data.name);
                $('.price-span').html(data.price);
                $('#unitPrice').val(data.price);
                $('#companyId').val(data.company_id);
                $('.available-balace').html(data.balance);
                $('#modal-default').modal('show');
            },
            error: function(){
                $('#line-chart').html("No data available");
            }
        });
    }

    function reloadTransaction(){
        $('#ownStockTbl').DataTable().ajax.reload();
    }

    $('#buyForm').submit(function(e){
        e.preventDefault();
        $.ajax({
            url: "stock/purchase",
            dataType: "json",
            data:{
                "_token": "{{ csrf_token() }}",
                "companyId":$('#companyId').val(),
                "totalUnit":$('#totalUnit').val(),
                "transactionType":$('#transactionType').val()
            },
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            success: function(data){
                if(data.success){
                    $('.customSuccess').html(data.message);
                    $('.alert-success').show();
                }
                else{
                    $('.customError').html(data.message);
                    $('.alert-danger').show();
                }
                
            },
            error: function(){
                $('.customError').html(data.message);
                $('.alert-danger').show();
            }
        });
    });
</script>
@endsection